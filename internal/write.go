package internal

import (
	"encoding/csv"
	"os"

	"github.com/360EntSecGroup-Skylar/excelize"
)

// WriteSummaryCsv takes a filepath to write to and the data to write in .csv format
func WriteSummaryCsv(outCsv string, sections []Section) error {
	out, err := os.Create(outCsv)
	defer out.Close()

	if err != nil {
		return err
	}

	csvOut := csv.NewWriter(out)
	for _, s := range sections {
		cont := append(s.Header(), s.Body()...)
		csvOut.WriteAll(cont)
	}

	return nil
}

// WriteSummaryXlsx takes a filepath to write to and the data to write in .xlsx format
func WriteSummaryXlsx(outXlsx string, sections []Section) {
	out := excelize.NewFile()
	activeSheet := out.GetSheetName(out.GetActiveSheetIndex())

	currentRow := 0
	for _, s := range sections {
		sec := append(s.Header(), s.Body()...)
		// Output the section header first
		for row := 0; row < len(sec); row++ {
			for col := 0; col < len(sec[0]); col++ {
				loc := genExcelLocation(currentRow, col)
				out.SetCellValue(activeSheet, loc, sec[row][col])
			}
			currentRow++
		}
	}

	out.SaveAs(outXlsx)
}
