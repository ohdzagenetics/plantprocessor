package internal

import (
	"testing"
)

var _ Section = new(SeedCountSection)
var _ Section = new(EventSummarySection)

func TestSeedCountSection(t *testing.T) {
	t.Run("Is empty at start", func(t *testing.T) {
		s := NewSeedCountSection()
		got := len(s.Body())
		exp := 0
		if got != exp {
			t.Errorf("Body had content at start. Got: %d, Expected: %d", got, exp)
		}
	})
	t.Run("Header is correct", func(t *testing.T) {
		s := NewSeedCountSection()
		got := s.Header()
		exp := [][]string{
			{"Event Number", "File", "Seedling Count", "Corrupt?"},
		}
		for ri, r := range exp {
			for ci := range r {
				if got[ri][ci] != exp[ri][ci] {
					t.Errorf("Invalid header.\n\tGot: %v\n\tExpected: %v", got, exp)
				}
			}
		}
	})
}

func TestEventSummarySection(t *testing.T) {
	t.Run("Is empty at start", func(t *testing.T) {
		s := NewEventSummarySection()
		got := len(s.Body())
		exp := 0
		if got != exp {
			t.Errorf("Body had content at start. Got: %d, Expected: %d", got, exp)
		}
	})
	t.Run("Header is correct", func(t *testing.T) {
		s := NewEventSummarySection()
		got := s.Header()
		exp := [][]string{
			{"Event Number", "ID", "Species", "Latitude", "Longitude", "Duplicate?"},
		}
		for ri, r := range exp {
			for ci := range r {
				if got[ri][ci] != exp[ri][ci] {
					t.Errorf(
						"Invalid header.\n"+
							"\tGot: %v\n"+
							"\tExpected: %v", got, exp)
				}
			}
		}
	})
}
