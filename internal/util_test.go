package internal

import (
	"testing"
)

// TestLocGeneration checks that the generateLoc function actually produces the correct Excel-style cell index
func TestLocGeneration(t *testing.T) {
	var flagtests = []struct {
		row int
		col int
		out string
	}{
		{0, 0, "A1"},
		{1, 0, "A2"},
		{2, 0, "A3"},
		{3, 0, "A4"},
		{4, 0, "A5"},
		{5, 0, "A6"},
		{0, 1, "B1"},
		{0, 2, "C1"},
		{0, 3, "D1"},
		{0, 4, "E1"},
		{0, 5, "F1"},
		{9, 6, "G10"},
	}

	for _, tt := range flagtests {
		exp := genExcelLocation(tt.row, tt.col)
		if exp != tt.out {
			t.Errorf("generateLoc(%d, %d)) => %q, want %q", tt.row, tt.col, exp, tt.out)
		}
	}
}
