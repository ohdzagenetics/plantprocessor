package internal

import (
	"sort"
	"strconv"
)

// Section is an output section with Header, Body, and the ability to AddRows
type Section interface {
	// A Section must be orderable
	sort.Interface

	// Header is the designation of what information is found in what column
	Header() [][]string

	// Body is the body rows, in other words all content below the Header
	Body() [][]string

	// AddRows will append the given rows, but keeps the output sorted according to the semantics of the section
	AddRows([][]string)
}

// SeedCountSection details the Event Number, File, Seedling Count, and whether
// the File was corrupt (i.e. unreadable)
type SeedCountSection struct {
	rows [][]string
}

// NewSeedCountSection produces a new empty SeedCountSection
func NewSeedCountSection() *SeedCountSection {
	return &SeedCountSection{
		rows: [][]string{},
	}
}

// Header is the designation of what information is found in what column
func (s *SeedCountSection) Header() [][]string {
	return [][]string{
		{"Event Number", "File", "Seedling Count", "Corrupt?"},
	}
}

// Body is the body rows, in other words all content below the Header
func (s *SeedCountSection) Body() [][]string {
	return s.rows
}

// AddRows appends new rows to the Body and sorts
func (s *SeedCountSection) AddRows(newrows [][]string) {
	s.rows = append(s.rows, newrows...)
	sort.Sort(s)
}

// Len gives the number of rows in the SeedCountSection
func (s *SeedCountSection) Len() int {
	return len(s.rows)
}

// Swap reorders rows in the SeedCountSection
func (s *SeedCountSection) Swap(i, j int) {
	s.rows[i], s.rows[j] = s.rows[j], s.rows[i]
}

// Less determines the proper (ascending) order of rows
func (s *SeedCountSection) Less(i, j int) bool {
	ithEvent, ithErr := strconv.ParseInt(s.rows[i][0], 0, 0)
	jthEvent, jthErr := strconv.ParseInt(s.rows[j][0], 0, 0)

	if ithErr == nil || jthErr == nil {
		return ithEvent < jthEvent // Sort by number
	}
	return s.rows[i][0] < s.rows[j][0] // Sort by letter
}

// EventSummarySection details the Event Number, ID, Species, Latitude, Longitude, Duplicate?
type EventSummarySection struct {
	rows [][]string
}

// NewEventSummarySection produces a new empty EventSummarySection
func NewEventSummarySection() *EventSummarySection {
	return &EventSummarySection{
		rows: [][]string{},
	}
}

// Header is the designation of what information is found in what column
func (e *EventSummarySection) Header() [][]string {
	return [][]string{
		{"Event Number", "ID", "Species", "Latitude", "Longitude", "Duplicate?"},
	}
}

// Body is the body rows, in other words all content below the Header
func (e *EventSummarySection) Body() [][]string {
	return e.rows
}

// AddRows appends new rows to the Body and sorts
func (e *EventSummarySection) AddRows(rows [][]string) {
	e.rows = append(e.rows, rows...)
	sort.Sort(e)
}

// Len gives the number of rows in the EventSummarySection
func (e *EventSummarySection) Len() int {
	return len(e.rows)
}

// Swap reorders rows in the EventSummarySection
func (e *EventSummarySection) Swap(i, j int) {
	e.rows[i], e.rows[j] = e.rows[j], e.rows[i]
}

// Less determines the proper (ascending) order of rows
func (e *EventSummarySection) Less(i, j int) bool {
	ithEvent, ithErr := strconv.ParseInt(e.rows[i][0], 0, 0)
	jthEvent, jthErr := strconv.ParseInt(e.rows[j][0], 0, 0)

	if ithErr == nil || jthErr == nil {
		return ithEvent < jthEvent // Sort by number
	}
	return e.rows[i][0] < e.rows[j][0] // Sort by letter
}
