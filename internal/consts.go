package internal

// Bounding box of Madagascar
const (
	minLatitude  = -25.6014344215
	minLongitude = 43.2541870461
	maxLatitude  = -12.0405567359
	maxLongitude = 50.4765368996
)
