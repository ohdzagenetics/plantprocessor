package internal

import (
	"strconv"
	"strings"
	"unicode"

	"github.com/360EntSecGroup-Skylar/excelize"
)

// genExcelLocation creates Excel-style cell column and row indexing of the form "A1" or "G10"
// Thus given a row and a column set (zero-indexed) it will generate the correct Excel index string
// for that set in the Excel's one and letter indexed value.
func genExcelLocation(row, col int) string {
	return string(col+65) + strconv.Itoa(row+1)
}

// centerText takes the text to center and the total width allowed to produce
// the text centered by space characters
func centerText(t string, w int) string {
	return centerTextWithFiller(t, w, ' ')
}

// centerText takes the text to center and the total width allowed to produce
// the text centered by teh given filler byte
func centerTextWithFiller(t string, w int, f byte) string {
	output := make([]byte, w)
	for i := range output {
		output[i] = f
	}
	diff := w - len(t)
	for i, r := range t {
		output[diff/2+i] = byte(r)
	}
	return string(output)
}

// getGPSSheet searches the Excel sheet names for the sheet containing our input data
func getGPSSheet(f *excelize.File) (gpsSheet string) {
	for _, sh := range f.GetSheetMap() {
		if strings.Contains(sh, "GPS") {
			gpsSheet = sh
			return
		}
	}
	return
}

// spaceMap removes all whitespace characters from a string (leading, internal, and trailing)
func spaceMap(str string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return -1
		}
		return r
	}, str)
}

// stringInSlice looks for a given string element in a slice of strings (likely replaceable by strings package function)
func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
