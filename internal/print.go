package internal

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gopkg.in/cheggaaa/pb.v1"
)

// PrintHeader prints a simple header for the commandline user
func PrintHeader() {
	var message string
	width, err := pb.GetTerminalWidth()
	if err != nil {
		message = fmt.Sprintf("\n%s\n%s\n%s\n",
			strings.Repeat("#", 80),
			strings.ToUpper(centerText(os.Args[0], 80)),
			strings.Repeat("#", 80),
		)
	} else {
		message = fmt.Sprintf("\n%s\n%s\n%s\n",
			strings.Repeat("#", width),
			strings.ToUpper(centerText(os.Args[0], width)),
			strings.Repeat("#", width),
		)
	}
	fmt.Println(message)
}

// PrintFooter prints a simple footer for the commandline user showing how many records (trees) were processed
func PrintFooter(count int) {
	var message string
	width, err := pb.GetTerminalWidth()
	if err != nil {
		message = fmt.Sprintf("\n%s\n%s\n%s\n",
			strings.Repeat("#", 80),
			strings.ToUpper(centerText(fmt.Sprintf("TOTAL RECORDS: %d", count), 80)),
			strings.Repeat("#", 80),
		)
	} else {
		message = fmt.Sprintf("\n%s\n%s\n%s\n",
			strings.Repeat("#", width),
			strings.ToUpper(centerText(fmt.Sprintf("TOTAL RECORDS: %d", count), width)),
			strings.Repeat("#", width),
		)
	}
	fmt.Println(message)
}

// PrintCurrentActivity lets the user know what file is currently being processed
func PrintCurrentActivity(filename string) {
	message := fmt.Sprintf("\nCURRENTLY PARSING: %s\n",
		filepath.Clean(filename),
	)
	fmt.Println(message)
}

// PrintCorruptFileInfo lets the user know that a file at a given path was corrupt
// This can generally happen via the file not being opened properly or
// the seed count was zero (should never be the case)
func PrintCorruptFileInfo(path string) {
	message := fmt.Sprintf("CORRUPT FILE? %s\n", path)
	fmt.Println(message)
}
