package internal

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"unicode"

	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/emirpasic/gods/sets/hashset"
	"github.com/pkg/errors"
)

// ProcessExcel reads an Excel (.xlsx) file and produces two Sections meant for output
func ProcessExcel(xlsxnum int, xlsxpath string, startNum int) (secs [2]Section, err error) {
	xlsxFile, err := excelize.OpenFile(xlsxpath)
	if err != nil {
		return secs, err
	}

	// Open Excel file and slice it into parts
	gpsSheet := getGPSSheet(xlsxFile)
	xlsxRows := xlsxFile.GetRows(gpsSheet)
	var allRows [][]string
	allRows = xlsxRows // the excelize library has a wonderful habit of causing concurrent map access past this point
	headerCount := headerCount(allRows)
	bodyRows := removeBlankRows(allRows[headerCount:])

	// Build event section
	secs[0] = extractEventSection(xlsxnum, bodyRows, startNum)

	// Build seed count section
	eventNumber := xlsxnum + 1 // Index by 1 for user output
	eventFile := filepath.Base(xlsxpath)
	secs[1] = extractSeedCountSection(bodyRows, eventNumber, eventFile)

	return
}

// withExtFrom takes a root path and a desired extension to produce a file list of all files with that
// extension below that root path (i.e. the directory is walked down to all children)
func withExtFrom(root, ext string) ([]string, error) {
	var files []string
	err := filepath.Walk(root, func(path string, f os.FileInfo, _ error) error {
		if !f.IsDir() {
			if filepath.Ext(path) == ext {
				absPath, err := filepath.Abs(path)
				if err == nil {
					files = append(files, absPath)
				} else {
					return err
				}
			}
		}
		return nil
	})
	return files, err
}

// CollectXlsx finds all .xlsx files within and below a given root path
func CollectXlsx(root string) ([]string, error) {
	return withExtFrom(root, ".xlsx")
}

func extractEventSection(xlsxnum int, bodyRows [][]string, startNum int) *EventSummarySection {
	eventRows := NewEventSummarySection()

	for _, row := range eventsContent(bodyRows, xlsxnum, startNum) {
		eventRows.rows = append(eventRows.rows, row)
	}
	return eventRows
}

func extractSeedCountSection(bodyRows [][]string, eventNumber int, eventFile string) *SeedCountSection {
	seedRows := NewSeedCountSection()
	seedlingCount := countSeeds(bodyRows)
	isCorrupt := true
	if seedlingCount != 0 {
		isCorrupt = false
	}

	seedRows.rows = append(seedRows.rows,
		[]string{strconv.Itoa(eventNumber), eventFile, strconv.Itoa(seedlingCount), strconv.FormatBool(isCorrupt)})
	return seedRows
}

// countSeeds determines the number of seed entries (i.e. the number of body rows)
// TODO: If only passing in body rows, headerCount() will always be zeros
func countSeeds(rows [][]string) int {
	seedCount := len(rows) - headerCount(rows)

	return seedCount
}

// headerCount determines the number of header rows, likely either one or two depending on existence of multirow headers
func headerCount(rows [][]string) (headnum int) {
	for _, cont := range rows {
		if hasHeaderElms(cont) && !hasNumbers(cont) {
			headnum++
		} else {
			break
		}
	}
	return
}

// hasHeaderElms checks for the existence of any known header elements in the input row
func hasHeaderElms(row []string) (found bool) {
	headers := []string{
		"GPS", "ID", "NAME", "LATITUDE", "LONGITUDE",
	}
	concat := strings.ToUpper(strings.Join(row, ""))
	for _, head := range headers {
		if strings.Contains(concat, head) {
			found = true
			break
		}
	}
	return
}

// hasNumbers determines if the row contains numbers,
// which likely correspond to GPS coordinates in decimal degree format
func hasNumbers(row []string) (found bool) {
	for _, cell := range row {
		for _, char := range cell {
			if unicode.IsDigit(char) {
				found = true
				break
			}
		}
	}
	return
}

// eventsContent extracts the ID, Species Name, Latitude, and Longitude of each seedling at an event
func eventsContent(bodyRows [][]string, eventnum int, startNum int) [][]string {
	content := [][]string{}
	idSet := hashset.New()

	for _, rowcontent := range bodyRows {
		var (
			num       = strconv.Itoa(eventnum + startNum) // Prep for user output making vent index 0 -> event number 1
			isDupStr  = "True"
			id, vname string
			lat, long float64
		)
		for _, c := range rowcontent {
			cns := spaceMap(c)
			cf, _ := strconv.ParseFloat(cns, 64)
			idMatch, _ := regexp.Match(`^\w+\.?\d+\.\d+\.\d+\.\d+$`, []byte(cns))
			vnameMatch, _ := regexp.Match(`^\D+$`, []byte(cns))
			switch {
			case idMatch:
				id = cns
			case vnameMatch:
				vname = cns
			case minLatitude < cf && cf < maxLatitude:
				lat = cf
			case minLongitude < cf && cf < maxLongitude:
				long = cf
			case cns == "":
				continue
			case 50 < cf && cf < 1000: // Elevation
				continue
			default:
				fmt.Fprintf(os.Stderr, "Unread data: no patterns were matched with: %v\n", cns)
			}
		}

		// Determine if the "ID" has never been seen
		if !idSet.Contains(id) {
			isDupStr = "False"
			idSet.Add(id)
		}

		// Bind the row content to the growing list of rows
		content = append(content,
			[]string{num,
				id,
				vname,
				strconv.FormatFloat(lat, 'f', -1, 64),
				strconv.FormatFloat(long, 'f', -1, 64),
				isDupStr,
			})
	}
	return content
}

// reassignIndexes overwrites the column indexing based on where the expected elements are found in the header rows
func reassignIndexes(indexes map[string]int, rows [][]string) (map[string]int, error) {
	for _, rowcontent := range rows {
		if hasHeaderElms(rowcontent) || !hasNumbers(rowcontent) {
			for name := range indexes {
				if is := indexStringArray(rowcontent, name); len(is) > 0 {
					for _, i := range is {
						if columnNotEmpty(rows, i) {
							indexes[name] = i
						}
					}
				}
			}
		}
	}
	for _, v := range indexes {
		if v < 0 {
			return nil, errors.New("Indexes were not set")
		}
	}
	return indexes, nil
}

func columnNotEmpty(rows [][]string, c int) bool {
	const rowsToCheck = 5
	var numFilled int
	if rowsToCheck < len(rows) {
		for i := 0; i < rowsToCheck; i++ {
			if c < len(rows[i]) && strings.TrimSpace(rows[i][c]) != "" {
				numFilled++
			}
		}
	}
	return numFilled > (rowsToCheck - numFilled) // Majority filled
}

func indexStringArray(strs []string, s string) (is []int) {
	sstd := strings.ToUpper(strings.TrimSpace(s))
	for i, v := range strs {
		cell := strings.ToUpper(strings.TrimSpace(v))
		if cell == sstd {
			is = append(is, i)
		}
	}
	return
}

// removeBlankRows removes any row with all blank cells
func removeBlankRows(bodyRows [][]string) [][]string {
	for i := 0; i < len(bodyRows); i++ {
		r := bodyRows[i]
		var blanks = 0
		for _, c := range r {
			if strings.TrimSpace(c) == "" {
				blanks++
			}
		}
		if blanks == len(r) { // If a completely empty row
			bodyRows = append(bodyRows[:i], bodyRows[i+1:]...)
			i--
			break
		}
	}
	return bodyRows
}
