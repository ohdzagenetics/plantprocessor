module bitbucket.org/ohdzagenetics/plantprocessor

go 1.13

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/emirpasic/gods v1.12.0
	github.com/fatih/color v1.7.0 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-runewidth v0.0.6 // indirect
	github.com/pkg/errors v0.8.1
	golang.org/x/sys v0.0.0-20191120155948-bd437916bb0e // indirect
	gopkg.in/cheggaaa/pb.v1 v1.0.28
)
