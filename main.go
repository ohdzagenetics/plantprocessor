/*
This program takes the many records of planting events as part of the MBP Reforestation Program/Conservation
Credit Program and creates a summary of all of these events with Event Number, number of seedlings planted, what Species
of tree was planted, where it was planted (Latitude and Longitude), as well as helpful variables like whether a file
was corrupt or whether a particular tree identifier has been duplicated.
*/
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"bitbucket.org/ohdzagenetics/plantprocessor/internal"
	"gopkg.in/cheggaaa/pb.v1"
)

var (
	wd, _    = os.Getwd()
	dir      = flag.String("directory", wd, "The directory where input .xlsx files are located")
	csvOut   = flag.String("csvOut", "", "Write a .csv with the given name")
	xlsxOut  = flag.String("xlsxOut", "", "Write a .xlsx with the given name")
	help     = flag.Bool("help", false, "Print the help message and exit")
	debug    = flag.Bool("debug", false, "Print debug messages throughout execution")
	startNum = flag.Uint("startNum", 1, "Starting event number")
)

func setup() {
	flag.Parse()

	// Exceptional cirsumstances that require an early exit.
	switch {
	case flag.NFlag() == 0:
		fmt.Println("No arguments provided")
		flag.Usage()
		os.Exit(2)
	case *help:
		flag.Usage()
		os.Exit(2)
	case len(*csvOut)+len(*xlsxOut) == 0:
		fmt.Println("At least one of -csvOut or -xlsxOut must be provided")
		flag.Usage()
		os.Exit(2)
	}
}

func debugMsg(format string, v ...interface{}) {
	if *debug {
		log.Printf(format, v...)
	}
}

func main() {
	debugMsg("Parsing CLI arguments")
	setup()

	debugMsg("Print header for user")
	internal.PrintHeader()

	debugMsg("Collect input .xlsx files")
	fullInputDir, err := filepath.Abs(*dir)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not get absolute path to %s\n", *dir)
		os.Exit(2)
	}
	xlsxFiles, err := internal.CollectXlsx(fullInputDir)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not collect .xlsx files %s\n", *dir)
		os.Exit(2)
	}

	debugMsg("Process files with progress bar and parallel for loop")
	bar := pb.StartNew(len(xlsxFiles))
	sections := make([][2]internal.Section, len(xlsxFiles))
	for i := range sections {
		sections[i] = *new([2]internal.Section)
	}
	sem := make(chan struct{}, len(xlsxFiles))
	for xlsxnum, xlsxpath := range xlsxFiles {
		go func(xlsxnum int, xlsxpath string) {
			if *debug {
				internal.PrintCurrentActivity(xlsxpath)
			}
			sections[xlsxnum], err = internal.ProcessExcel(xlsxnum, xlsxpath, int(*startNum))
			if err != nil {
				relpath, _ := filepath.Rel(wd, xlsxpath)
				fmt.Fprintf(os.Stderr, "Failed to process %q\n", relpath)
			}

			sem <- struct{}{}
			bar.Increment()
		}(xlsxnum, xlsxpath)
	}
	for i := 0; i < len(xlsxFiles); i++ {
		<-sem
	}
	bar.FinishPrint("Finished processing .xlsx files")

	debugMsg("Building master sections")
	masterSeedSection := internal.NewSeedCountSection()
	masterEventSection := internal.NewEventSummarySection()
	for _, f := range sections {
		for _, s := range f {
			switch s.(type) {
			case *internal.SeedCountSection:
				masterSeedSection.AddRows(s.Body())
			case *internal.EventSummarySection:
				masterEventSection.AddRows(s.Body())
			}
		}
	}

	debugMsg("Preparing master for output")
	master := []internal.Section{masterSeedSection, masterEventSection}

	debugMsg("Writing master to output")
	if *xlsxOut != "" {
		debugMsg("Writing the .xlsx output")
		internal.WriteSummaryXlsx(*xlsxOut, master)
	}

	if *csvOut != "" {
		debugMsg("Writing the .csv output")
		internal.WriteSummaryCsv(*csvOut, master)
	}

	debugMsg("Print footer for user")
	internal.PrintFooter(masterEventSection.Len())

	return
}
