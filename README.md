# Plant Processor

This plant processor takes `.xlsx` files containing information on reforestations efforts and produces a summary `.csv` and/or `.xlsx` file detailing the number of seedlings for each planting event (i.e., file), whether IDs were duplicated, and other factors of general importance.

## Running

The program is runnable via:

```bash
go run !(*_test).go
```

Running this command alone produces a help message detailing functionality and flag options.

## Dependencies

All dependencies can be retrieved via:

```bash
go get ./...
```
